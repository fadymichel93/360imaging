package TestCases;

import Pages.LoginPage;
import Pages.RegisterPage;
import org.junit.Assert;
import org.testng.annotations.Test;
import utils.ExcelFileManager;

import java.io.IOException;

public class LoginTest extends TestBase{

    @Test
   public void ValidateToLogin() throws IOException, InterruptedException {
       loginPage.setUsernameInput(RegisterTest.Email);
       loginPage.setPasswordInput(externalFile(0,4));
       loginPage.setLoginButton();
       Assert.assertEquals(loginPage.getTitle(),externalFile(0,6));
   }

}
