package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends PageBase {
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    private By usernameInput = By.name("username");
    private By passwordInput = By.name("password");
    private By loginButton = By.xpath("//button[normalize-space()='Login']");


    public void setUsernameInput(String text) {
        setText(text, usernameInput);
    }

    public void setPasswordInput(String pass) {
        setText(pass, passwordInput);
    }

    public void setLoginButton() {
        clickButton(loginButton);
    }

    public String getTitle() throws InterruptedException {
        return getUrl();
    }
}
