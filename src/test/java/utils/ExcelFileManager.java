package utils;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public  class ExcelFileManager {


    public static XSSFWorkbook workbook;
    public static XSSFSheet sheet;
    public static XSSFCell cell;
    public static XSSFRow Row;
    public static File file;

    public static String setExcelFile(int ColumnNumber, int rowNumber) throws IOException {
        file = new File(Constants.FilePath);
        FileInputStream inputStream = new FileInputStream(file);
        workbook = new XSSFWorkbook(inputStream);
        sheet = workbook.getSheet("Sheet1");
        Row = sheet.getRow(rowNumber);
        cell = Row.getCell(ColumnNumber);
        String Data = cell.getStringCellValue();
        return Data;
    }
}
