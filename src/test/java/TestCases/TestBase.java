package TestCases;

import Pages.LoginPage;
import Pages.RegisterPage;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;
import utils.ExcelFileManager;


import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TestBase {
    static WebDriver driver;
    protected String URL;
    protected RegisterPage registerPage;
    protected LoginPage loginPage;
    protected ExcelFileManager fileManager;

    @BeforeSuite
    public void setUp() throws IOException {

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.navigate().to(URL = externalFile(0, 5));

    }

    @BeforeMethod
    public  void init() {
        registerPage = new RegisterPage(driver);
        loginPage = new LoginPage(driver);
    }

    @AfterSuite
    public void tearDown() {

        //  driver.quit();
    }

    public static void REFRESH() {
        driver.navigate().refresh();
    }

    public String externalFile(int column, int row) throws IOException {
        return fileManager.setExcelFile(column, row);
    }

    @AfterMethod
    public void testResult(ITestResult result) {
        String filename = new SimpleDateFormat("ddMMyyHHmm").format(new Date());

        if (result.getStatus() == ITestResult.SUCCESS) {
        } else if (result.getStatus() == ITestResult.FAILURE) {
            try {
                TakesScreenshot ts = (TakesScreenshot) driver;
                File source = ts.getScreenshotAs(OutputType.FILE);
                FileUtils.copyFile(source, new File(System.getProperty("user.dir") + "/Screenshots/" + result.getName() + filename + ".png"));
            } catch (Exception e) {
                System.out.println("Exception while taking screenshot.... " + e.getMessage());

            }
        }
    }
}

