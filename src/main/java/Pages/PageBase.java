package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;


public class PageBase {
    protected static WebDriver driver;
    protected WebDriverWait wait;

    public PageBase(WebDriver driver) {
        this.driver = driver;
    }

    protected WebElement Action(By locator) {
        return driver.findElement(locator);
    }

    protected void waitElement(By locator) {
        wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        wait.until(ExpectedConditions.elementToBeClickable(locator));
    }

    protected void setText(String text, By locator) {
        waitElement(locator);
        Action(locator).clear();
        Action(locator).sendKeys(text);
    }

    protected void clickButton(By locator) {
        waitElement(locator);
        Action(locator).click();


    }

    protected boolean getErrorMessage(By locator) {
        waitElement(locator);
        return Action(locator).isDisplayed();
    }

    public String getUrl() throws InterruptedException {
        Thread.sleep(3000);
        return driver.getCurrentUrl();

    }


}
