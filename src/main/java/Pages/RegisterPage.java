package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;



import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegisterPage extends PageBase {
    public RegisterPage(WebDriver driver) {
        super(driver);
    }

    private By firstName = By.name("firstname");
    private By lastName = By.name("lastname");
    private By mobileNumber = By.name("phone");
    private By email = By.name("email");
    private By password = By.name("password");
    private By confirmPassword = By.name("confirmpassword");
    private By signUpButton = By.cssSelector("form[method='POST'] button[type='submit']");
    private By errorMessage = By.xpath("//*[@id=\"headersignupform\"]/div[2]/div");
    private By logOutDDL = By.xpath("//*[@id=\"dropdownCurrency\"]/i");
    private By logOut = By.linkText("Logout");


    public void setFirstName(String Name) {
        setText(Name, firstName);
    }

    public void setLastName(String lName) {
        setText(lName, lastName);
    }

    public void setMobileNumber(String number) {
        setText(number, mobileNumber);
    }

    public void setEmail(String Email) {
        setText(Email, email);
    }

    public void setPassword(String pass) {
        setText(pass, password);
    }

    public void setConfirmPassword(String pass) {
        setText(pass, confirmPassword);
    }

    public void setSignUpButton() {
        clickButton(signUpButton);
    }




    public static boolean isValidPass(String password) {
        {

            String pass = "^(?=.*[0-9])"
                    + "(?=.*[a-z])(?=.*[A-Z])"
                    + "(?=.*[@#$%^&+=])"
                    + "(?=\\S+$).{8,32}$";

            Pattern pattern = Pattern.compile(pass);
            if (password == null) {
                return false;
            }
            Matcher m = pattern.matcher(password);
            return m.matches();
        }


    }

    public boolean errorMessageIsDisplayed() {

        return getErrorMessage(errorMessage);

    }

    public void LogOut(){
        clickButton(logOut);

    }

    public void setLogOutDDL(){
        clickButton(logOutDDL);
    }

}
