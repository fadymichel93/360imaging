package TestCases;

import Pages.RegisterPage;
import com.github.javafaker.Faker;
import org.junit.Assert;
import org.testng.annotations.Test;
import utils.ExcelFileManager;

import java.io.IOException;

public class RegisterTest extends TestBase {
    ExcelFileManager fileManager;
    static Faker faker = new Faker();
    public static String Email = faker.internet().emailAddress();


    @Test(priority = 1)
    public void ValidateToRegisterWithInvalidPassword() throws IOException {
        for (int i = 1; i < 5; i++) {
            fileManager = new ExcelFileManager();
            registerPage.setFirstName(externalFile(0, 1));
            registerPage.setLastName(externalFile(0, 2));
            registerPage.setMobileNumber(externalFile(0, 3));
            registerPage.setEmail(Email);
            registerPage.setPassword(externalFile(1, i));
            registerPage.setConfirmPassword(externalFile(1, i));
            registerPage.setSignUpButton();
            Assert.assertTrue(registerPage.errorMessageIsDisplayed());
            Assert.assertFalse(registerPage.isValidPass(externalFile(1, i)));
            REFRESH();
        }

    }

    @Test(priority = 2, dependsOnMethods = "ValidateToRegisterWithInvalidPassword")
    public void ValidateToRegister() throws IOException, InterruptedException {
        registerPage = new RegisterPage(driver);
        registerPage.setFirstName(externalFile(0, 1));
        registerPage.setLastName(externalFile(0, 2));
        registerPage.setMobileNumber(externalFile(0, 3));
        registerPage.setEmail(Email);
        registerPage.setPassword(externalFile(0, 4));
        registerPage.setConfirmPassword(externalFile(0, 4));
        registerPage.setSignUpButton();
        Assert.assertEquals(registerPage.getUrl(),externalFile(0, 6));
        Assert.assertTrue(registerPage.isValidPass(externalFile(0, 4)));
        System.out.println(Email);
    }


    @Test(priority = 3)
    public void ValidateToLogOut(){
        registerPage.setLogOutDDL();
        registerPage.LogOut();
    }
}




